ZAD�N�
Word Count souboru
Typick� probl�m s jednoduchou paralelizac�. Na vstupu je sada souboru, na v�stupu je serazen� v�pis obsa�en�ch slov a jejich cetnost.

WORD COUNTER
Tento program slou�� k poc�t�n� slov v souboru a ulo�en� n�sleduj�c� statistiky do souboru statistics.txt.
Pro sna��� pr�stup ke zkompilovan�mu programu je ve slo�ce semestralka k dispozici semestralka.exe, pres kter� je mo�n� program spustit.

STRUKTURA
word_item
	- uk�l�d� statistiku pro ka�d� slovo pres v�echny soubory, pres kter� se statistika vytv�r�
letter
	- ukl�d� serazen� vector word_items, tedy slov zac�naj�c� na stejn� p�smeno
alphabet
	- ukl�d� vector letters, tedy seznam v�ech p�smen v abecebe (velk�ch a mal�ch)
word_parser
	- pou�it k parsov�n� vstupn�ch souboru a n�sledn�mu ukl�d�n� do alphabet

V�STUP MEREN�
Aplikace by testov�n� na Windows 10, procesor: Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz, 2808 Mhz, j�dra: 4, logick� procesory: 8
pocet souboru       doba behu singlethread (s)    doba behu multithread (s)
     1                   0,7390				0,752
     2			 1,575				1,547
     3			 2,397				2,378
     4			 2,935				2,913
     5			 3,444				3,401
Z v�sledku je patrn�, �e je potreba vet�� objem dat, aby byl rozd�l mezi singlethread a multithread patrn�.