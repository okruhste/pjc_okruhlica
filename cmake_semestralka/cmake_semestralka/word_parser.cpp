#include "word_parser.h"


void word_parser::parseFileIntoWordsStatistic()
{
	std::ifstream file(this->filename);
	if (file.is_open()) {
		std::string line;
		std::string word;
		while (getline(file, line)) {
			std::string::iterator iter = line.begin();
			while (iter != line.end()) {
				int value = (int)*iter;
				if ((value >= 65 && value <= 90) || (value >= 97 && value <= 122) || value == 39) {
					word += *iter;
				}
				else if (value == 45) {
					if (word.size() != 0) {
						word += *iter;
					}
				}
				else {
					if (word.size() != 0) {
						this->alpha->increment(word, this->index);
						word = "";
					}
				}
				iter++;
			}
			if (word.size() != 0) {
				this->alpha->increment(word, this->index);
				word = "";
			}
		}
		if (word.size() != 0) {
			this->alpha->increment(word, this->index);
			word = "";
		}
		file.close();
	}
}

alphabet * word_parser::getAlphabet()
{
	return this->alpha;
}

int word_parser::getIndex()
{
	return	this->index;
}

std::string word_parser::getFilename()
{
	return this->filename;
}


