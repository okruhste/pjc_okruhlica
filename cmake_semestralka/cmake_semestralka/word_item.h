#pragma once
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

class word_item {
public:
	word_item(std::string w, const int size) {
		this->word = w;
		for (int i = 0; i < size; i++) {
			frequency.push_back(0);
		}
	}

	~word_item() = default;

	std::string getWord();

	void increment(int index);

	long int getTotalNumberOfOccurancesOfWord();

	std::string toString(const int length);
private:
	std::string word;
	std::vector<signed long int> frequency;
};
