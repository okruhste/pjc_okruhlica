#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <memory>
#include <mutex>  
#include "word_item.h"

class letter {
public:
	letter(const int size) {
		this->numberOfFiles = size;
		this->longestWord = 0;
	}

	~letter() = default;

	void increment(std::string word, const int file);

	long int getTotalNumberOfWordsByLetter();

	void printStatistic(std::string file, const int longestWord); //param bude input stream of some kind

	int getLongestWord();

private:
	int numberOfFiles;
	std::vector<word_item*> words;
	int longestWord;
};