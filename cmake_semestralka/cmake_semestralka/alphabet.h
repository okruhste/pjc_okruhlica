#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <mutex>  
#include <deque>
#include "letter.h"

class alphabet {
public:
	alphabet(const int _numberOfFiles) {
		for (int i = 0; i < 52; i++) {
			this->letters.push_back(letter(_numberOfFiles));
		}
		this->numberOfFiles = _numberOfFiles;
	}

	~alphabet() = default;

	void increment(const std::string word, const int file);

	void printStatistic(); //prints statitstic, needs inputstream and file

	int getLengthOfLongestWord();

private:
	std::vector<letter> letters;
	int numberOfFiles;
};