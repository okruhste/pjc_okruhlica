#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <mutex>  
#include "alphabet.h"

class word_parser {
public:
	word_parser(alphabet * a, const int i, const std::string file) {
		this->alpha = a;
		this->index = i;
		this->filename = file;
	}

	~word_parser() = default;

	void parseFileIntoWordsStatistic();

	alphabet * getAlphabet();

	int getIndex();

	std::string getFilename();

private:
	alphabet * alpha;
	int index;
	std::string filename;
};