#include "word_item.h"

std::string word_item::getWord()
{
	return this->word;
}

void word_item::increment(int index)
{
	frequency[index]++;
}

long int word_item::getTotalNumberOfOccurancesOfWord()
{
	long int total = 0;
	for (int i = 0; i < this->frequency.size(); i++) {
		total += this->frequency[i];
	}
	return total;
}

std::string word_item::toString(const int length)
{
	std::ostringstream stream;
	stream << std::setw(length) << std::left << this->word;
	double total = 0;
	for (int i = 0; i < this->frequency.size(); i++) {
		stream << std::setw(length) << std::right << this->frequency[i];
		total += this->frequency[i];
	}
	stream << std::setw(length) << std::right << total;
	return stream.str();
}
