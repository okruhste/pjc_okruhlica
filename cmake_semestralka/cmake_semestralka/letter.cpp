#include "letter.h"

std::mutex mtx;

void letter::increment(std::string word, const int file)
{
	std::lock_guard<std::mutex> lock(mtx);
	if (this->words.size() == 0) {
		word_item * item = new word_item(word, this->numberOfFiles);
		this->words.push_back(item);
		this->words.at(0)->increment(file);
	}
	else {
		std::vector<word_item *>::iterator iter = this->words.begin();
		if (word.length() > this->longestWord) {
			this->longestWord = word.length();
		}
		while (iter != this->words.end()) {
			int comparison = (*iter)->getWord().compare(word);
			if (comparison < 0) {
				iter++;
			}
			else if (comparison == 0) {
				(*iter)->increment(file);
				return;
			}
			else {
				std::vector<word_item *>::iterator inserted = this->words.insert(iter, new word_item(word, this->numberOfFiles));
				(*inserted)->increment(file);
				return;
			}
		}
	}
}

long int letter::getTotalNumberOfWordsByLetter()
{
	long int total = 0;
	std::vector<word_item *>::iterator iter = this->words.begin();
	while (iter != this->words.end()) {
		total += (*iter)->getTotalNumberOfOccurancesOfWord();
		iter++;
	}
	return total;
}

void letter::printStatistic(std::string file, const int longestWord)
{
	std::ofstream outfile;
	outfile.open(file, std::ios::out | std::ios::app);
	if (outfile.is_open())
	{
		std::vector<word_item *>::iterator iter = this->words.begin();
		while (iter != this->words.end()) {
			outfile << (*iter)->toString(longestWord) << "\n";
			iter++;
		}
		outfile.close();
	}

}

int letter::getLongestWord()
{
	return this->longestWord;
}

