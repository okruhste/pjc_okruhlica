#include "alphabet.h"

void alphabet::increment(const std::string word, const int file)
{
	int first = (int)word.at(0);
	if (first >= 65 && first <= 90) {
		this->letters.at(first - 65).increment(word, file);
	}
	else if (first >= 97 && first <= 122) {
		this->letters.at(first - 71).increment(word, file);
	}
	return;
}

void alphabet::printStatistic()
{
	int longestWord = this->getLengthOfLongestWord();
	if (longestWord < 7) {
		longestWord = 7;
	}
	std::ostringstream stream;
	stream << std::setw(longestWord) << std::left << "";
	for (int i = 0; i < numberOfFiles; i++) {
		std::string tmp = "file " + std::to_string(i + 1);
		stream << std::setw(longestWord) << std::right << tmp;
	}
	stream << std::setw(longestWord) << std::right << "total" << "\n";

	std::string filename = "statistics.txt";
	std::ofstream outfile;
	outfile.open(filename, std::ios::out | std::ios::trunc);
	if (outfile.is_open())
	{
		outfile << stream.str();
		outfile.close();
	}

	long int total = 0;
	std::vector<letter>::iterator iter = this->letters.begin();
	while (iter != this->letters.end()) {
		(*iter).printStatistic(filename, longestWord);
		total += (*iter).getTotalNumberOfWordsByLetter();
		iter++;
	}
	stream.str(std::string());
	stream << std::setw(longestWord) << std::left << "";
	for (int i = 0; i < numberOfFiles; i++) {
		stream << std::setw(longestWord) << std::right << "";
	}

	stream << std::setw(longestWord) << std::right << total;

	outfile.open(filename, std::ios::out | std::ios::app);
	if (outfile.is_open())
	{
		outfile << "\n";
		outfile << stream.str() << "\n";
		outfile.close();
	}
}

int alphabet::getLengthOfLongestWord()
{
	std::vector<letter>::iterator iter = this->letters.begin();
	int tmp = 0;
	while (iter != this->letters.end()) {
		if ((*iter).getLongestWord() > tmp) {
			tmp = (*iter).getLongestWord();
		}
		iter++;
	}
	return tmp;
}

