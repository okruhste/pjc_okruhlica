﻿// semestralka.cpp : Tento soubor obsahuje funkci main. Provádění programu se tam zahajuje a ukončuje.
//

#include "cmake_semestralka.h"
#include "alphabet.h"
#include "word_parser.h"
#include <iostream>
#include <ctime>    
#include <regex>
#include <deque>


const char* HELP = "--help";
const char* MULTI = "-mt";
const char* TEXT = ".txt";

int main(int argc, char* argv[]) {
	std::clock_t begin = clock();
	if (argc == 2 && std::strcmp(argv[1], HELP) == 0) {
		std::cout << "This program is an implementation of a word counter.\n";
		std::cout << "After you supply it with text files, it will count their letters and will create a new text file statitstics.txt, which will contain the full statistic of used words in the supplemented text files.\n";
		std::cout << "-mt     will cause the program to run as a multithred\n";
		std::cout << "example:\n";
		std::cout << "   semestralka filename.txt:\n";
		std::cout << "   semestralka filename1.txt filename2.txt\n";
		std::cout << "   semestralka -mt filename1.txt filename2.txt\n";
	}
	else if (argc >= 2) {
		//*****************************************************************************************************MULTITHREAD
		if (std::strcmp(argv[1], MULTI) == 0 && argc > 2) {
			bool isValid = true;
			for (int i = 2; i < argc; i++) {
				if (std::strlen(argv[i]) < 5 || !std::regex_match(argv[i], std::regex(".+txt"))) {
					isValid = false;
				}
			}
			if (isValid) {
				std::cout << "Multithread mode\n";
				std::vector<std::string> filenames;
				for (int i = 2; i < argc; i++) {
					filenames.push_back(std::string(argv[i]));
				}
				alphabet * alpha = new alphabet(filenames.size());
				std::vector<word_parser> parsers;
				for (int i = 0; i < filenames.size(); i++) {
					parsers.push_back(word_parser(alpha, i, filenames.at(i)));
				}
				std::deque<std::thread> threads;
				for (int i = 0; i < filenames.size(); i++) {
					threads.push_back(std::thread(&word_parser::parseFileIntoWordsStatistic, &parsers.at(i)));
					threads.at(i).join();
				}
				alpha->printStatistic();
				std::clock_t end = clock();
				double time = double(end - begin) / CLOCKS_PER_SEC;
				std::cout << "Elapsed time: " + std::to_string(time) + " ms\n";
			}
			else {
				std::cout << "Invalid call! Invalid used of parameters. Parametrs should be text files. Consult --help.\n";
			}
		}
		//*****************************************************************************************************SINGLETHREAD
		else {
			bool isValid = true;
			for (int i = 1; i < argc; i++) {
				if (std::strlen(argv[i]) < 5 || !std::regex_match(argv[i], std::regex(".+txt"))) {
					isValid = false;
				}
			}
			if (isValid) {
				std::cout << "Sinlgethread mode\n";
				std::vector<std::string> filenames;
				for (int i = 1; i < argc; i++) {
					filenames.push_back(std::string(argv[i]));
				}
				alphabet * alpha = new alphabet(filenames.size());
				for (int i = 0; i < filenames.size(); i++) {
					word_parser * tmp = new word_parser(alpha, i, filenames.at(i));
					tmp->parseFileIntoWordsStatistic();
				}
				alpha->printStatistic();
				std::clock_t end = clock();
				double time = double(end - begin) / CLOCKS_PER_SEC;
				std::cout << "Elapsed time: " + std::to_string(time) + " ms\n";
			}
			else {
				std::cout << "Invalid call! Invalid used of parameters. Parametrs should be text files. Consult --help.\n";
			}
		}
	}
	else {
		std::cout << "Invalid call! Invalid used of parameters.\n";
	}
	return 0;
}

// Spuštění programu: Ctrl+F5 nebo nabídka Ladit > Spustit bez ladění
// Ladění programu: F5 nebo nabídka Ladit > Spustit ladění

// Tipy pro zahájení práce:
//   1. K přidání nebo správě souborů použijte okno Průzkumník řešení.
//   2. Pro připojení ke správě zdrojového kódu použijte okno Team Explorer.
//   3. K zobrazení výstupu sestavení a dalších zpráv použijte okno Výstup.
//   4. K zobrazení chyb použijte okno Seznam chyb.
//   5. Pokud chcete vytvořit nové soubory kódu, přejděte na Projekt > Přidat novou položku. Pokud chcete přidat do projektu existující soubory kódu, přejděte na Projekt > Přidat existující položku.
//   6. Pokud budete chtít v budoucnu znovu otevřít tento projekt, přejděte na Soubor > Otevřít > Projekt a vyberte příslušný soubor .sln.
